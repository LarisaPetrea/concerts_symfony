<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\concerts;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/llista", name="llistat")
     */
    public function listAction(Request $request)
    {

        $concerts = $this->getDoctrine()->getRepository('AppBundle:concerts')->findAll();

        return $this->render('default/list.html.twig', array(
            'concerts' => $concerts,
        ));
    }


    /**
     * @Route("/insereix", name="insert")
     */
    public function insertAction(Request $request)
    {

        
        $concerts = new concerts();
        $concerts->setNom('Nombre del concierto.');
        $concerts->setArtista('Nombre del artista.');

        $form = $this->createFormBuilder($concerts)
            ->add('nom', TextType::class)
            ->add('artista', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Insereix concerts'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $task = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($task);
            $entityManager->flush();

            return $this->redirectToRoute('llistat');
        }

        return $this->render('default/insert.html.twig', array(
            'form' => $form->createView(),
        ));
    }


    /**
     * @Route("/busca", name="buscar")
     */
    public function searchAction(Request $request)
    {

        
        $concerts = new concerts();
        $concerts->setNom('Nombre del concierto.');

        $form = $this->createFormBuilder($concerts)
            ->add('nom', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Insereix concerts'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $task = $form["nom"]->getData();

            $concerts = $this->getDoctrine()->getRepository('AppBundle:concerts')->findBy(array('nom' => $task));

            $result = "";

            foreach ($concerts as $concert => $value){
                $result = $value;
            }

            return $this->render('default/search.html.twig', array(
                'result' => $result,
                'form' => $form->createView(),
            ));
        }

        return $this->render('default/search.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/borra", name="erase")
     */
    public function eraseAction(Request $request)
    {

        
        $concerts = new concerts();
        $concerts->setNom('Nombre del concierto.');

        $form = $this->createFormBuilder($concerts)
            ->add('nom', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Elimina concerts'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $task = $form["nom"]->getData();

            $concerts = $this->getDoctrine()->getRepository('AppBundle:concerts')->findBy(array('nom' => $task));

            $entityManager = $this->getDoctrine()->getManager();

            $concert = $concerts[0];

            $entityManager-> remove($concert);
            

            $entityManager->flush();
            

            return $this->redirectToRoute('llistat');
        }

        return $this->render('default/erase.html.twig', array(
            'form' => $form->createView(),
        ));
    }

}
